/**
 * @param {HTMLElement} parentElement
 */
export function removeChildren (parentElement) {
  while (parentElement.firstElementChild) {
    parentElement.removeChild(parentElement.firstElementChild)
  }
}