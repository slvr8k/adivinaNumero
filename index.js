import { removeChildren } from './utils/removeChildren.js'

// Le añadimos a la ventana un evento para que cargue la función menú una vez termine de cargar el DOM
window.addEventListener('DOMContentLoaded', menu)
const secretNum = Math.floor(Math.random() * 100) + 1;
let count = 0
const toor = document.getElementById('toor')
let finish = 0

function menu () {
  const basic = createBtnBasic('Básico')
  const advance = createBtnAdvance('Avanzado')

  toor.appendChild(document.createElement('p')).textContent = 'Seleccione el modo de juego'
  toor.appendChild(basic)
  toor.appendChild(document.createElement('br'))
  toor.appendChild(advance)
}

// ######################################################################################
// Las siguientes líneas se encargan de generar los 2 modos de juego (básico y avanzado)
// ######################################################################################
function createBtnBasic (textContent) {
  const btn = document.createElement('button')
  btn.textContent = textContent
  btn.id = 'basicGame'
  btn.addEventListener('click', basicClick)

  return btn
}

function createBtnAdvance (textContent) {
  const btn = document.createElement('button')
  btn.textContent = textContent
  btn.id = 'advanceGame'
  btn.addEventListener('click', advanceClick)

  return btn
}

function basicClick (event) {
  const toor = document.getElementById('toor')
  removeChildren(toor)
  document.getElementById("avisos").hidden = false
  document.getElementById("num").hidden = false
  document.getElementById("enviarNum").hidden = false
}

function advanceClick (event) {
  const toor = document.getElementById('toor')
  removeChildren(toor)
  const buttons = createButtons()
  toor.appendChild(buttons)
}

// ######################################################################
// Las siguientes líneas se encargan del control del juego en modo básico
// ######################################################################

document.getElementById("enviarNum").addEventListener('click', tryNumber)
function tryNumber() {
  let num = document.getElementById("num").value
  const acertar = `<p>FELICIDADES ACERTASTE EL NÚMERO</p>`
  const numMayor = `<p>El número es mayor a ${num} </p>`
  const numMenor = `<p>El número es menor a ${num} </p>`
  const trys = `<p>Has necesitado un total de ${count} intentos para acertar el número secreto</p>`

  // Vaciamos el contenido del <p> "avisos" de nuestro HTML
  document.getElementById("avisos").innerHTML = ""

  // Realizamos las comprobaciones con el número recibido
  if (validar(num)) {
    if (num == secretNum) {
      document.getElementById("avisos").innerHTML += acertar
      document.getElementById("avisos").innerHTML += trys
    } else if (num > secretNum) {
      document.getElementById("avisos").innerHTML += numMenor
    } else {
      document.getElementById("avisos").innerHTML += numMayor
    }

    count++;
  }
}

// Función de validación del número introducido
function validar(num) {
  if (!/^[1-9][0-9]?$|^100$/.test(document.getElementById("num").value)) {
    document.getElementById("avisos").innerHTML = `Por favor introduzca un número entre 1 y 100`;
    return false;
  }

  return true;
}

// ########################################################################
// Las siguientes líneas se encargan del control del juego en modo avanzado
// ########################################################################

function createButtons() {
  const fragment = document.createDocumentFragment()

  for (let i = 1; i <= 100; i++) {
    const btn = document.createElement('button')
    btn.textContent = i
    btn.id = i
    btn.addEventListener('click', e => {
      checkBtn(secretNum, i)
    })
    fragment.appendChild(btn)
  }

  return fragment
}

function checkBtn(secretNum, i){
  document.getElementById("avisos").hidden = false
  const trys = `<p>Has necesitado un total de ${count} intentos para acertar el número secreto</p>`
  const acertar = `<p>FELICIDADES ACERTASTE EL NÚMERO SECRETA ERA ${secretNum}</p>`
  const numMayor = `<p>El número es mayor a ${i} </p>`
  const numMenor = `<p>El número es menor a ${i} </p>`
  document.getElementById("avisos").innerHTML = ""
  if (i == secretNum ){
    document.getElementById("avisos").innerHTML += acertar
    document.getElementById("avisos").innerHTML += trys
    document.getElementById("avisos").style.color = "#7ce77c"
    document.getElementById(i).style.backgroundColor = "#718078"
    finish++

    endGame()
  } else{
    document.getElementById(i).style.backgroundColor = "#4a4063"
    document.getElementById(i).style.color = "#d95d39"
    document.getElementById("avisos").style.color = "white"
    if (i > secretNum) {
      document.getElementById("avisos").innerHTML += numMenor
    } else {
      document.getElementById("avisos").innerHTML += numMayor
    }
    count++
  }  
}

function endGame(){
  if(finish > 0){
    for (let i = 1; i <= 100; i++) {
      document.getElementById(i).style.pointerEvents = "none"
      }
    }
}


